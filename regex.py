"""
Sobre regex

CARACTERE       DESCRIÇAO                                                   EXEMPLOS
[]              Define um range ou grupo de caracteres                      [0-9] [a-z] [abc]
*               Marca nenhuma, uma ou mais ocorrências                      sol*
{}              Quantidade de repetições de uma ocorrência definida         [abc]{5}
\d              Qualquer número de 0 até 9                                  \d{3,4}
\w              Qualquer número de 0 até 9, letra de a até z ou _           \w{10}
|               Um ou outro                                                 @|$
()              Captura e agrupa                                            (\w{4})
"""


import re

# o padrão é algum número seguido de uma letra seguido de outro número
padrao = "[0-9][a-z][0-9]"
texto = "123 1a2 1cc aa1"
resposta = re.search(padrao, texto)
print(resposta.group())

# o padrão é algum número seguido de duas letra seguido de outro número
padrao = "[0-9][a-z]{2}[0-9]"
texto = "123 1ab2 1cc aa1"
resposta = re.search(padrao, texto)
print(resposta.group())


# a primeira parte do e-mail deve ser um texto de 2 a 50 caracteres exemplo: jorge.rabello3
# seguido de um @
# a segunda parte deve ser um texto de 3 a 10 caracteres exemplo: gmail
# seguido de um .
# a quarta parte deve ser um texto de 2 a 3 caracteres exemplo: com
# seguido de um .
# a quinta e última parte deve ser um texto de 2 a 3 caracteres exemplo: br

padrao_email = "\w{2,50}@\w{2,15}\.[a-z]{2,3}\.?([a-z]{2,3})?"
email = "jorgerabello3@gmail.com.br"
resposta = re.search(padrao_email, email)
print(resposta.group())


padrao_telefone = "[0-9]{2}[0-9]{4,5}[0-9]{4}"
texto = "eu gosto do número 11951482377"

resposta = re.findall(padrao_telefone, texto)
print(resposta)
