from validate_docbr import CPF as lib_cpf


class CPF:
    def __init__(self, documento):
        documento = str(documento)
        if self.validate(documento):
            self.cpf = documento
        else:
            raise ValueError("CPF Inválido")

    def __str__(self):
        return self.format()

    def validate(self, cpf):
        validador = lib_cpf()
        return validador.validate(cpf)

    def format(self):
        mascara = lib_cpf()
        return mascara.mask(self.cpf)
