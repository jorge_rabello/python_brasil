import requests


class ClientIBGE:
    def __init__(self, cep):
        self.cep = cep
        self.url = "https://viacep.com.br/ws/"

    def consulta_api(self):
        url_consulta = self.url + self.cep + "/json"
        dados = requests.get(url_consulta).json()

        return (
            dados["bairro"],
            dados["localidade"],
            dados["uf"],
        )
