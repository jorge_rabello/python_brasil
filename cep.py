from client_ibge import ClientIBGE


class CEP:
    def __init__(self, cep):
        if self.validate(cep):
            self.cep = cep
        else:
            raise ValueError("CEP Inválido")

    def __str__(self):
        return self.format()

    def validate(self, cep):
        if len(cep) == 8:
            return True
        else:
            return False

    def format(self):
        return "{}-{}".format(self.cep[:5], self.cep[5:])

    def consulta_cep(self):
        client = ClientIBGE(self.cep)
        return client.consulta_api()
