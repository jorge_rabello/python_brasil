from cep import CEP
from datas import Datas
from documento import Documento
from telefone import Telefone

documento_cpf = Documento.cria_documento("36421147844")
documento_cnpj = Documento.cria_documento("26482599000130")

print(documento_cpf)
print(documento_cnpj)


telefone = Telefone("5511951482377")
print(telefone)

cadastro = Datas()

print(cadastro.momento_cadastro)
print(cadastro.mes_de_cadastro())
print(cadastro.dia_da_semana())
print(cadastro.format_data())
print(cadastro.tempo_de_caastro())
print(cadastro)


cep = CEP("07094190")
print(cep)

print(cep.consulta_cep())

bairro, cidade, uf = cep.consulta_cep()

print(f"Bairro: {bairro}")
print(f"Cidade: {cidade}")
print(f"UF: {uf}")
