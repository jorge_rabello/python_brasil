import requests

response = requests.get("https://viacep.com.br/ws/01001000/json/")
print(response)
print(response.json())
print(response.text)

print(type(response.text))  # str
print(type(response.json()))  # dict

bairro = response.json()["bairro"]
print(f"Bairro: {bairro}")
