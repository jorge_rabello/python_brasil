format:
	isort $(or $(filter-out $@,$(MAKECMDGOALS)), .)
	black $(or $(filter-out $@,$(MAKECMDGOALS)), .)
