import re


class Telefone:
    def __init__(self, telefone):
        if self.validate(telefone):
            self.telefone = telefone
        else:
            raise ValueError("Número de telefone inválido")

    def __str__(self):
        return self.format()

    def validate(self, telefone):
        pattern = "([0-9]{2,3})?([0-9]{2})?([0-9]{4,5})([0-9]{4})"
        resposta = re.findall(pattern, telefone)
        if resposta:
            return True
        else:
            return False

    def format(self):
        pattern = "([0-9]{2,3})?([0-9]{2})([0-9]{4,5})([0-9]{4})"
        resposta = re.search(pattern, self.telefone)
        numero_formatado = "+{} ({}) {}-{}".format(
            resposta.group(1), resposta.group(2), resposta.group(3), resposta.group(4)
        )
        return numero_formatado
