from validate_docbr import CNPJ as lib_cnpj


class CNPJ:
    def __init__(self, documento):
        documento = str(documento)
        if self.validate(documento):
            self.cnpj = documento
        else:
            raise ValueError("CNPJ Inválido")

    def __str__(self):
        return self.format()

    def validate(self, cnpj):
        validador = lib_cnpj()
        return validador.validate(cnpj)

    def format(self):
        mascara = lib_cnpj()
        return mascara.mask(self.cnpj)
