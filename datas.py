from datetime import datetime, timedelta


class Datas:
    def __init__(self):
        self.momento_cadastro = datetime.today()

    def __str__(self):
        return self.format_data()

    def mes_de_cadastro(self):
        meses_do_ano = [
            "Janeiro",
            "Fevereiro",
            "Março",
            "Abril",
            "Maio",
            "Junho",
            "Julho",
            "Agosto",
            "Setembro",
            "Outubro",
            "Novembro",
            "Dezembro",
        ]
        mes_cadastro = self.momento_cadastro.month - 1
        return meses_do_ano[mes_cadastro]

    def dia_da_semana(self):
        dias_da_semana = ["Segunda", "Terça", "Quarta", "Quinta", "Sexta"]
        return f"{dias_da_semana[self.momento_cadastro.weekday()]}-Feira"

    def format_data(self):
        return self.momento_cadastro.strftime("%d/%m/%Y %H:%M")

    def tempo_de_caastro(self):
        return datetime.today() + timedelta(days=30) - self.momento_cadastro
